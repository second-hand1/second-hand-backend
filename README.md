## Getting started

### Install all package

On your terminal

```Javascript
    npm install

```

## Database

- `sequelize db:create` : to create a database
- `sequelize db:migrate` : to run database migration
- `sequelize db:drop` : to delete a database

## Run Server

On your terminal

```Javascript
    npm start

```
