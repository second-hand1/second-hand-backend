'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Transaksis', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      kd_transaksi: {
        allowNull: false,
        unique: true,
        type: Sequelize.STRING
      },
      id_barang: {
        type: Sequelize.INTEGER,
        unique: false,
        references: {
            model: "Barangs",
            key: "id"
        },
      },
      id_pembayar: {
        type: Sequelize.INTEGER,
        unique: false,
        references: {
            model: "Users",
            key: "id"
        }
      },
      id_tawaran: {
        type: Sequelize.INTEGER,
        unique: true,
        references: {
            model: "Tawarans",
            key: "id"
        },
      },
      nominal_bayar: {
        type: Sequelize.INTEGER,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Transaksis');
  }
};