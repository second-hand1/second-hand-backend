'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Notifikasis', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      judul: {
        type:Sequelize.TEXT
      },
      deskripsi: {
        type:Sequelize.TEXT
      },
      id_user: {
        type: Sequelize.INTEGER,
        unique: true,
        references: {
            model: "Users",
            key: "id"
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      },
      id_tawaran: {
        type: Sequelize.INTEGER,
        unique: true,
        references: {
            model: "Tawarans",
            key: "id"
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Notifikasis');
  }
};