'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Tawarans', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      harga_tawaran: {
        type: Sequelize.INTEGER
      },
      id_buyer: {
        type: Sequelize.INTEGER,
        unique: false,
        references: {
            model: "Users",
            key: "id"
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      },
      id_barang: {
        type: Sequelize.INTEGER,
        unique: false,
        references: {
            model: "Barangs",
            key: "id"
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      },
      status: {
        type: Sequelize.ENUM('disetujui', 'pending', 'tidak disetuji')
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Tawarans');
  }
};