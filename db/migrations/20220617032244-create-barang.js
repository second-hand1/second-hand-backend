'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Barangs', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      id_seller: {
        type: Sequelize.INTEGER,
        unique: false,
        references: {
            model: "Users",
            key: "id"
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      },
      nama_produk: {
        type: Sequelize.TEXT
      },
      harga_produk: {
        type: Sequelize.INTEGER
      },
      kategori: {
        type: Sequelize.STRING
      },
      deskripsi: {
        type: Sequelize.TEXT
      },
      status: {
        type: Sequelize.ENUM('ditawar', 'terjual')
      },
      foto_produk: {
        type: Sequelize.TEXT
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Barangs');
  }
};