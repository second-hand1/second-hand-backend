const express = require("express");
const path = require("path");
const controllers = require("../app/controllers");
const usersController = controllers.usersController;
const authController = controllers.authController;

const router = express.Router();

router.get("/api/cities", (req, res) => {
  res.sendFile(path.join(__dirname, '../db/masterdata/cities.json'));
})

router.route("/api/users/register")
  .post(usersController.userRegister);

router.route("/api/login")
  .post(authController.login);

router.route("/api/users/list")
  .get(usersController.getAllUsers);

router.route("/api/users/inputProfile")
  .put(authController.authorize, usersController.inputProfile);

router.route("/api/users/profile")
  .get(authController.authorize, usersController.getProfile);

router.route("/api/users/delete/:id")
  .delete(authController.authorize, usersController.deleteUser);


  
module.exports = router;