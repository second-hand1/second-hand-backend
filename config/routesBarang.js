const express = require("express");
const path = require("path");
const controllers = require("../app/controllers");
const authController = controllers.authController;
const barangController = controllers.barangController;
const multer = require("../app/midleware/multer");



const router = express.Router(); 

router.route("/api/barang/create")
  .post(authController.authorize, barangController.createBarang);

router.route("/api/barang/update/:id")
  .put(authController.authorize, barangController.updateBarang);

router.route("/api/barang/list")
  .get(barangController.getBarang);

router.route("/api/barang/list1")
  .get(barangController.getBarang1);

router.route("/api/barang/list/:kategori")
  .get(barangController.getBarangByKategori);

router.route("/api/barang/list1/:kategori")
  .get(barangController.getBarangByKategori1);

router.route("/api/barang/list-terjual")
  .get(authController.authorize, barangController.getBarangTerjual);

router.route("/api/barang/listku")
  .get(authController.authorize,barangController.getBarangByIdSeller);

router.route("/api/barang/:id")
  .get(barangController.getBarangById);

router.route("/api/barang/delete/:id")
  .delete(authController.authorize, barangController.deleteBarang);


  

module.exports = router;