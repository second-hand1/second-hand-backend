const express = require("express");
const path = require("path");
const controllers = require("../app/controllers");
const transaksiController = controllers.transaksiController;
const authController = controllers.authController;


const router = express.Router();

router.route("/api/transaksi/:id_barang/:id_tawaran")
  .post(authController.authorize, transaksiController.uploadTransaksi);

router.route("/api/transaksi-saya")
  .get(authController.authorize, transaksiController.getByIdBuyer);

router.route("/api/transaksi/barang/:id_barang")
  .get(authController.authorize, transaksiController.getByIdBarang);

router.route("/api/transaksi/sold")
  .get(authController.authorize, transaksiController.getByIdSeller);



module.exports = router;