const express = require("express");
const path = require("path");
const controllers = require("../app/controllers");
const tawaranController = controllers.tawaranController;
const authController = controllers.authController;


const router = express.Router();

router.route("/api/tawaran/upload/:id_barang")
  .post(authController.authorize, tawaranController.uploadTawaran);

router.route("/api/tawaran/terima/:id/:id_barang")
  .post(tawaranController.tawaranDiterima);

router.route("/api/tawaran/list")
  .get(tawaranController.getAllTawaran);

router.route("/api/tawaran/barang/:id_barang")
  .get(tawaranController.getByIdBarang);

router.route("/api/tawaran/users")
  .get(authController.authorize, tawaranController.getByIdUser);

router.route("/api/tawaran/status-barang")
  .get(authController.authorize, tawaranController.getByStatusBarang);

router.route("/api/tawaran/barangku")
  .get(authController.authorize, tawaranController.getByIdSeller);

router.route("/api/tawaran/:id")
  .get(authController.authorize, tawaranController.getByIdTawaran);

router.route("/api/tawaran/delete/:id")
  .delete(tawaranController.deleteTawaran);


module.exports = router;