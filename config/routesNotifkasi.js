const express = require("express");
const path = require("path");
const controllers = require("../app/controllers");
const authController = controllers.authController;
const notifikasiController = controllers.notifikasiController;

const router = express.Router();

router.route("/api/notifikasi")
  .post(authController.authorize, notifikasiController.uploadNotifikasi)
  .get(authController.authorize, notifikasiController.getAllNotifikasi);

router.route("/api/notifikasi/:id")
  .get(notifikasiController.getNotifikasiById)

router.route("/api/notifikasi/delete/:id")
  .delete(authController.authorize, notifikasiController.deleteNotifikasi)

  module.exports = router;