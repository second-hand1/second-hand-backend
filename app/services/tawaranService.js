const { Tawaran } = require("../models");
const { Users } = require("../models");
const { Barang } = require("../models");

const create = (id_barang, value) => Tawaran.create(id_barang, value);
const getAllTawaran = async () => {
    try {
      const fotoproduk  = await Tawaran.findAll({include: [
        Users, Barang
      ]});
      return fotoproduk;
    } catch (err) {
      throw err;
    }
  };
const getByIdBarang = (id_barang) => Tawaran.findAll({where: { id_barang }, include: [
  Users, Barang
]});
const getByIdBarangPending = (id_barang) => Tawaran.findAll({where: { status: 'pending', id_barang }, raw: true});
const getByIdUser = (id_buyer) => Tawaran.findAll({where: { id_buyer }, include: [
  Users, Barang
]});
const getByIdTawaran = (id) => Tawaran.findOne({where: { id }, include: [
  Users, Barang
]});
const tawaranDiterima = (id, value) => Tawaran.update(value, {where: { id }});
const deleteTawaran = (id) => Tawaran.destroy({ where: { id } });
const getByIdSeller = async (id_seller) => Tawaran.findAll({ where: { status: 'pending' }, include:[ 
  Users, {model:Barang, where:{ id_seller }}
]});
const getByStatusBarang = async (status, id_seller) => Tawaran.findAll({where: { status: 'disetujui' }, include:[
  Users, {model:Barang, where:{ status, id_seller }}
]});
module.exports = {
    create,
    getAllTawaran,
    getByIdBarang,
    getByIdBarangPending,
    getByIdUser,
    getByIdTawaran,
    deleteTawaran,
    tawaranDiterima,
    getByIdSeller,
    getByStatusBarang
};