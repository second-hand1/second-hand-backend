const { Users } = require("../models");

const create = (value) => Users.create(value); 
const getAllUsers = async () => {
  try {
    const users  = await Users.findAll();
    return users;
  } catch (err) {
    throw err;
  }
}; 
const getByEmail = (email) => Users.findOne({ where: { email } });  
const get = (id) => Users.findOne({ where: { id } });
const update = (id, value) => Users.update(value, {where: { id }});
const deleteUser = (id) => Users.destroy({ where: { id } });


module.exports = {
  create,
  getByEmail,
  get,
  update,
  deleteUser,
  getAllUsers

};
