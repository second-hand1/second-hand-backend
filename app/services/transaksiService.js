const { Transaksi } = require("../models");
const { Tawaran } = require("../models");
const { Barang } = require("../models");
const { Users } = require("../models");

const create = (id_barang, id_tawaran, value) => Transaksi.create(id_barang, id_tawaran, value);
const findAll =  async () => {
    try {
      const transaksi  = await Transaksi.findAll({include: [
        Barang
      ]});
      return transaksi;
    } catch (err) {
      throw err;
    }
  };
const getByIdBuyer = (id_buyer) => Transaksi.findAll({ include:[
    Barang, {model:Tawaran, where:{ id_buyer }, include:[ Users ]}
]});
const getByIdBarang = (id_barang) => Transaksi.findAll({where: { id_barang }, include:[
    Barang
]});
const getByIdKdTransaksi = (kd_transaksi) => Transaksi.findAll({where: { kd_transaksi }, include:[
    Barang, Users, Tawaran
]});
const getByIdSeller = async (id_seller) => Transaksi.findAll({ include:[
    {model:Barang, where:{ id_seller }}, {model:Tawaran, include:[ Users ]}
], where: {  }});

    



module.exports = {
    create,
    getByIdBuyer,
    getByIdBarang,
    findAll,
    getByIdSeller
}