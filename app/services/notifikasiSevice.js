const { Notifikasi } = require("../models");
const { Tawaran } = require("../models");
const { Barang } = require("../models");
const { Users } = require("../models");

const create = (value) => Notifikasi.create(value);

const getNotifikasiById = (id) => Notifikasi.findOne({where: { id }, include: [
  Users, {model:Tawaran, include: [ Barang ] }]});

const getAllNotifikasi = async (id_user) => {
  try {
    const notifikasi = await Notifikasi.findAll({where: { id_user }, include: [
      Users, {model:Tawaran, include: [ Barang ] }
    ]});
    return notifikasi;
  } catch (err) {
    throw err;
  }
};

const deleteNotifikasi = (id) => Notifikasi.destroy({ where: { id } });

module.exports = {
  create,
  getAllNotifikasi,
  deleteNotifikasi,
  getNotifikasiById
};
