const { Barang } = require("../models");
const { Users } = require("../models");
var sequelize = require('sequelize');
const create = async (value) =>{
  try {
    const barang = await Barang.create(value)
    return barang;
  } catch (err) {
    throw err;
  }
};

const listBarang = async () => {
  try {
    const barang = await Barang.findAll({where: { status: 'ditawar' }, include: [
      {model:Users, where:{ id: {
        [sequelize.Op.not]: 18
       } }}
    ]});
    return barang;
  } catch (err) {
    throw err;
  }
};

const listBarang1 = async () => {
  try {
    const barang = await Barang.findAll({where: { status: 'ditawar', id: {
      [sequelize.Op.not]: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26]
     }
     }, include: [
      Users
    ]});
    return barang;
  } catch (err) {
    throw err;
  }
};

const listBarangTerjual = async (id_seller) => {
  try {
    const barang = await Barang.findAll({where: { status: 'terjual', id_seller }, include: [
      Users, 
    ]});
    return barang;
  } catch (err) {
    throw err;
  }
};
const listBarangByKategori = async (kategori) => {
  try {
    const barang = await Barang.findAll({where: { status: 'ditawar', kategori }, include: [
      {model:Users, where:{ id: {
        [sequelize.Op.not]: 18
       } }}
    ]});
    return barang;
  } catch (err) {
    throw err;
  }
};
const listBarangByKategori1 = async (kategori) => {
  try {
    const barang = await Barang.findAll({where: { status: 'ditawar', kategori, id: {
      [sequelize.Op.not]: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26]
     } }, include: [
      Users
    ]});
    return barang;
  } catch (err) {
    throw err;
  }
};

const getBarang = (id) => Barang.findOne({ where: { id }, include: [
  Users ]});
const getBarangByIdSeller = (id_seller) => Barang.findAll({ where: { id_seller }, include: [
  Users ]});
const update = (id, value) => Barang.update(value, {where: { id }});
const deleteBarang = (id) => Barang.destroy({ where: { id } });

module.exports = {
  create,
  listBarang1,
  listBarang,
  listBarangTerjual,
  getBarang,
  getBarangByIdSeller,
  update,
  deleteBarang,
  listBarangByKategori,
  listBarangByKategori1
};
