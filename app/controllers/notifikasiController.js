const service = require("../services/notifikasiSevice");

const uploadNotifikasi = async (req, res) => {   
    const { id_user, judul, deskripsi, id_tawaran } = req.body;
   
    const newNotifikasi = {
        id_user, 
        judul, 
        deskripsi, 
        id_tawaran 
    };
    
    service.create(newNotifikasi)
    .then((notifikasi) => {
      res.status(201).json({
        status: "success",
        data: notifikasi,
      });
    })
    .catch(err => {
      res.status(422).json({
        status: "error",
        message: err.message,
      });
    })
  }

  const getAllNotifikasi = (req, res) => {
    const id_user  = req.user.id;
      service
      .getAllNotifikasi(id_user)
      .then((notifikasi) => {
        res.status(200).json({
          status: "success",
          data: notifikasi,
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "error",
          message: err.message,
        });
      });
    };
  const getNotifikasiById = (req, res) => {
    const { id }  = req.params;
      service
      .getNotifikasiById(id)
      .then((notifikasi) => {
        res.status(200).json({
          status: "success",
          data: notifikasi,
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "error",
          message: err.message,
        });
      });
    };
  
  const deleteNotifikasi = async (req, res) => {
    const { id } = req.params;
    const getNotifikasi = await service.getNotifikasi(id);
    if(getNotifikasi == null) {
      res.status(422).json({
          status: "error",
          message: "Notifikasi tidak ada"
      });
      return;
    }else{
    service
      .deleteNotifikasi(id)
      .then(() => {
        res.status(200).json({
          status: "success",
          message: "Notifikasi deleted successfully",
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "error",
          message: err.message,
        });
      });
    }
  };
    module.exports = {
        uploadNotifikasi,
        getAllNotifikasi,
        getNotifikasiById,
        deleteNotifikasi
    };