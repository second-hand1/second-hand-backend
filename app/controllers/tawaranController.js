const service = require("../services/tawaranService");
const servicebarang = require("../services/barangService");

const uploadTawaran = async (req, res) => {   
    const { harga_tawaran } = req.body;
    const id_buyer = req.user.id;
    const id_barang = req.params.id_barang;
    const status = "pending";
    const getbarang = await servicebarang.getBarang(id_barang);
    if(!getbarang) {
      res.status(404).json({
          status: "error",
          message: "barang tidak ditemukan"
      });
      return;
    };
    const id_seller = await getbarang.id_seller;
      
   if(id_buyer == id_seller) {
      res.status(422).json({
          status: "error",
          message: "tidak bisa menawar barang sendiri"
      });
      return;
  }else{
    const newTawaran = {
      harga_tawaran,
      id_buyer,
      id_barang,
      status
    };
    
    service.create(newTawaran)
    .then((barang) => {
      res.status(201).json({
        status: "success",
        data: barang,
      });
    })
    .catch(err => {
      res.status(422).json({
        status: "error",
        message: err.message,
      });
    })
  }
  };

  const getAllTawaran = (req, res) => {
    service
      .getAllTawaran()
      .then((tawaran) => {
        res.status(200).json({
          status: "success",
          data: tawaran,
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "error",
          message: err.message,
        });
      });
  };
  
  const getByIdBarang = (req, res) => {
    const { id_barang } = req.params;
    service
      .getByIdBarang(id_barang)
      .then((tawaran) => {
        res.status(200).json({
          status: "success",
          data: tawaran,
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "error",
          message: err.message,
        });
      });
  };

  const getByIdUser = (req, res) => {
    const id_buyer  = req.user.id
    service
      .getByIdUser(id_buyer)
      .then((tawaran) => {
        res.status(200).json({
          status: "success",
          data: tawaran,
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "error",
          message: err.message,
        });
      });
  };
  
  const tawaranDiterima = async (req, res) => {
    const { id, id_barang } = req.params
    const status = "tidak disetuji";
    const status1 = "disetujui";
    const updateTawaran = {
      status: status1
    };
    await service.tawaranDiterima(id, updateTawaran);
    const getbarang = await service.getByIdBarangPending(id_barang);
    if (!getbarang) {
      return;
    }
    for (i = 0; i < getbarang.length; i++) {
      const updateTawaran = {
          status
        };
      await service.tawaranDiterima(getbarang[i].id, updateTawaran)
      
    }
      res.status(200).json({
        status: "success",
        message: "tawaran diterima",
      });
  };

  const getByIdSeller = (req, res) => {
    const id_seller  = req.user.id
    service
      .getByIdSeller(id_seller)
      .then((tawaran) => {
        res.status(200).json({
          status: "success",
          data: tawaran,
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "error",
          message: err.message,
        });
      });
  };
  const getByStatusBarang = (req, res) => {
    const status  = "terjual";
    const id_seller  = req.user.id;
    service
      .getByStatusBarang(status, id_seller)
      .then((tawaran) => {
        res.status(200).json({
          status: "success",
          data: tawaran,
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "error",
          message: err.message,
        });
      });
  };

  const getByIdTawaran = (req, res) => {
    const { id }  = req.params
    service
      .getByIdTawaran(id)
      .then((tawaran) => {
        res.status(200).json({
          status: "success",
          data: tawaran,
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "error",
          message: err.message,
        });
      });
  };

  const deleteTawaran = (req, res) => {
    const { id } = req.params;
    service
      .deleteTawaran(id)
      .then(() => {
        res.status(200).json({
          status: "success",
          message: "Tawaran deleted successfully",
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "error",
          message: err.message,
        });
      });
  };
    module.exports = {
        uploadTawaran,
        getAllTawaran, 
        getByIdBarang, 
        getByIdUser,
        getByIdSeller,
        getByIdTawaran,
        tawaranDiterima,
        getByStatusBarang,
        deleteTawaran
    };