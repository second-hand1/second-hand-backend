const usersController = require("./usersController");
const barangController = require("./barangController");
const tawaranController = require("./tawaranController");
const authController = require("./authController");
const transaksiController = require("./transaksiController");
const notifikasiController = require("./notifikasiController");

module.exports = {
  barangController,
  usersController,
  tawaranController,
  authController,
  transaksiController,
  notifikasiController
};
