const service = require("../services/userService");
const bcrypt = require("bcrypt")
var salt = bcrypt.genSaltSync(10);
const cloudinary = require("../thirdparty/cloudinary");

const getAllUsers = (req, res) => {
  service
    .getAllUsers()
    .then((tawaran) => {
      res.status(200).json({
        status: "success",
        data: tawaran,
      });
    })
    .catch((err) => {
      res.status(422).json({
        status: "error",
        message: err.message,
      });
    });
};
const userRegister = async (req, res) => {
  const user = await service.getByEmail(req.body.email)
  if(user){
    res.status(403).json({
      status: "error",
      message: "email sudah terdaftar",
    });
    return;
  
  }
  const { nama, email } =  req.body;
    const password = bcrypt.hashSync(req.body.password, salt);
    const newUsers = {
      nama,
      email,
      password
    };
    service.create(newUsers)
    .then(() => {
      res.status(201).json({
        status: "success",
        message: "User berhasil terdaftar",
      });
    })
    .catch(err => {
      res.status(422).json({
        status: "error",
        message: err.message,
      });
    })
  };
  const inputProfile = async (req, res) => {
    const {
      id
    } = req.user;
    const {
      nama,
      kota,
      alamat, 
    } = req.body;
    const no_hp = "+62" + req.body.no_hp;
    console.log("files: ", req.files)
    var data_foto = req.files.length - 1;
    const data = [];
    for (i = 0; i <= data_foto; i++) {
      const result = await cloudinary.uploader.upload(req.files[i].path);
      const hasil = result.url;
      console.log("hasil: ", result.url)
       data.push(hasil);
    }
    console.log("data: ", data)
  const foto_user = data[0];
    const updateUser = {
      nama,
      kota,
      alamat, 
      no_hp,
      foto_user
    };
    service
      .update(id, updateUser)
      .then((user) => {
        res.status(200).json({
          status: "success",
          message: "Input user successfully",
          data: user
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "error",
          message: err.message,
        });
      });
  };

  
  const deleteUser = async (req, res) => {
    const { id }  = req.params;
    service
      .deleteUser(id)
      .then(() => {
        res.status(200).json({
          status: "success",
          message: "User deleted successfully",
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "error",
          message: err.message,
        });
      });
  };
  const getProfile = async (req, res) => {
    const { id }  = req.user;
    service
      .get(id)
      .then((user) => {
        res.status(200).json({
          status: "success",
          data: user
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "error",
          message: err.message,
        });
      });
  }

  module.exports = {
    userRegister,
    deleteUser,
    inputProfile, 
    getAllUsers,
    getProfile
    
  };