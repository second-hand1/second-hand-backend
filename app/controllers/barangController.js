const service = require("../services/barangService");
const cloudinary = require("../thirdparty/cloudinary");

const createBarang = async (req, res) => {
  const {
    nama_produk,
    kategori,
    deskripsi,
  } = req.body;
  const strHarga = await req.body.harga_produk;
  console.log("req: ",req);
    var data_foto = req.files.length;
    console.log("datafoto: ", data_foto)
    const data = [];
    for (i = 0; i < data_foto; i++) {
      const result = await cloudinary.uploader.upload(req.files[i].path);
      const hasil = result.url;
      console.log("hasil :",hasil); 
      data.push(hasil);
    }
    console.log(data);
  const harga_produk = parseInt(strHarga.replace(/[^0-9]/g, ""));
  const foto_produk = JSON.stringify(data);
  const id_seller = req.user.id;
  const status = "ditawar";
  const newBarang = {
    nama_produk,
    id_seller,
    harga_produk,
    kategori,
    deskripsi,
    status,
    foto_produk,
  };
  service
    .create(newBarang)
    .then((barang) => {
      return res.status(200).json({
        status: "success",
        data: barang,
      });
    })
    .catch((err) => {
      return res.status(422).json({
        status: "error",
      });
    });
};

const getBarang = (req, res) => {
  service
    .listBarang()
    .then((barang) => {
      res.status(200).json({
        status: "success",
        data: barang,
      });
    })
    .catch((err) => {
      res.status(422).json({
        status: "error",
        message: err.message,
      });
    });
};
const getBarang1 = (req, res) => {
  service
    .listBarang1()
    .then((barang) => {
      res.status(200).json({
        status: "success",
        data: barang,
      });
    })
    .catch((err) => {
      res.status(422).json({
        status: "error",
        message: err.message,
      });
    });
};
const getBarangTerjual = (req, res) => {
  const id_seller = req.user.id
  service
    .listBarangTerjual(id_seller)
    .then((barang) => {
      res.status(200).json({
        status: "success",
        data: barang,
      });
    })
    .catch((err) => {
      res.status(422).json({
        status: "error",
        message: err.message,
      });
    });
  };
const getBarangByKategori = (req, res) => {
  const { kategori } = req.params;
  service
    .listBarangByKategori(kategori)
    .then((barang) => {
      res.status(200).json({
        status: "success",
        data: barang,
      });
    })
    .catch((err) => {
      res.status(422).json({
        status: "error",
        message: err.message,
      });
    });
};
const getBarangByKategori1 = (req, res) => {
  const { kategori } = req.params;
  service
    .listBarangByKategori1(kategori)
    .then((barang) => {
      res.status(200).json({
        status: "success",
        data: barang,
      });
    })
    .catch((err) => {
      res.status(422).json({
        status: "error",
        message: err.message,
      });
    });
};
const getBarangById = async (req, res) => {
  const { id }  = req.params;
  const barang = await service.getBarang(req.params.id);
  if (!barang) {
    res.status(404).json({
      status: "error",
      message: "Barang tidak ditemukan"
    });
    return;
  }
  service
    .getBarang(id)
    .then((barang) => {
      res.status(200).json({
        status: "success",
        data: barang,
      });
    })
    .catch((err) => {
      res.status(422).json({
        status: "error",
        message: err.message,
      });
    });
};
const getBarangByIdSeller = async (req, res) => {
  const id_seller = req.user.id;
  service
    .getBarangByIdSeller(id_seller)
    .then((barang) => {
      res.status(200).json({
        status: "success",
        data: barang,
      });
    })
    .catch((err) => {
      res.status(422).json({
        status: "error",
        message: err.message,
      });
    });
};

const updateBarang = async (req, res) => {
  const {
    id
  } = req.params;
  const {
    nama_produk,
    kategori,
    deskripsi,
  } = req.body;
  const strHarga = await req.body.harga_produk;
  var data_foto = req.files.length;
  console.log("datafoto: ", data_foto)
  const data = [];
  for (i = 0; i < data_foto; i++) {
      const result = await cloudinary.uploader.upload(req.files[i].path);
      const hasil = result.url;
      console.log("hasil :",hasil); 
      data.push(hasil);
  }
  console.log(data);
  const harga_produk = parseInt(strHarga.replace(/[^0-9]/g, ""));
  const foto_produk = JSON.stringify(data);
  const updateBarang = {
    nama_produk,
    harga_produk,
    kategori,
    deskripsi,
    foto_produk
  };
  service
    .update(id, updateBarang)
    .then((barang) => {
      res.status(200).json({
        status: "success",
        message: "Barang updated successfully",
        data: barang
      });
    })
    .catch((err) => {
      res.status(422).json({
        status: "error",
        message: err.message,
      });
    });
};

const deleteBarang = (req, res) => {
  const {
    id
  } = req.params;
  service
    .deleteBarang(id)
    .then(() => {
      res.status(200).json({
        status: "success",
        message: "Barang deleted successfully",
      });
    })
    .catch((err) => {
      res.status(422).json({
        status: "error",
        message: err.message,
      });
    });
};

module.exports = {
  createBarang,
  getBarang,
  getBarang1,
  updateBarang,
  deleteBarang,
  getBarangById,
  getBarangByIdSeller,
  getBarangTerjual,
  getBarangByKategori,
  getBarangByKategori1
};