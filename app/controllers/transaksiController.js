const service = require("../services/transaksiService");
const servicetawaran = require("../services/tawaranService");
const servicebarang = require("../services/barangService");

const uploadTransaksi = async (req, res) => {   
    const { nominal_bayar } = req.body;
    const { id_barang, id_tawaran } = req.params;
    const status = "terjual";
    const id_pembayar = req.user.id;
    const kd_transaksi = new Date().getTime() + id_tawaran.toString();
    const transaksi = await servicetawaran.getByIdTawaran(id_tawaran);
    const getbarang = await servicebarang.getBarang(id_barang);
    const transaksibarang = await service.getByIdBarang(id_barang);
    const harga = await transaksi.harga_tawaran;
    const id_seller = await getbarang.id_seller;
    const barang = transaksibarang;
    console.log(nominal_bayar);
    if(nominal_bayar != harga){
        res.status(422).json({
            status: "error",
            message: "nominal bayar tidak sesuai"
        });
        return;
    }
    // else if(id_pembayar != id_seller){
    //       res.status(422).json({
    //           status: "error"
    //       });
    //       return;
    //   }
    // // else if(barang_ditemukan == ""){
    // //     res.status(422).json({
    // //         status: "error",
    // //         message: "barang sudah tidak ada"
    // //     });
    // //     return;
    // // }
    // else if(barang != ""){
    //     res.status(422).json({
    //         status: "error",
    //         message: "barang sudah tidak ada"
    //     });
    //     return;
    // }
    else{
            const newTransaksi = {
                kd_transaksi,
                id_barang,
                id_pembayar,
                id_tawaran,
                nominal_bayar
            };
            const updateBarang = {
                status
            };
            servicebarang.update(id_barang, updateBarang);
            
            service.create(newTransaksi)
            .then((transaksi) => {
              res.status(201).json({
                status: "success",
                data: transaksi,
              });
            })
            .catch(err => {
              res.status(422).json({
                status: "error",
                message: err.message,
              });
            })
    }
  };
  const getByIdBuyer = (req, res) => {
    service
      .getByIdBuyer(req.user.id)
      .then((transaksi) => {
        res.status(200).json({
          status: "success",
          data: transaksi,
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "error",
          message: err.message,
        });
      });
  };
  const getByIdBarang = (req, res) => {
    service
      .getByIdBarang(req.params.id_barang)
      .then((transaksi) => {
        res.status(200).json({
          status: "success",
          data: transaksi,
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "error",
          message: err.message,
        });
      });
  };
  const getByIdSeller = (req, res) => {
    service
      .getByIdSeller(req.user.id)
      .then((transaksi) => {
        res.status(200).json({
          status: "success",
          data: transaksi,
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "error",
          message: err.message,
        });
      });

      
  };
  module.exports = { 
    uploadTransaksi,
    getByIdBuyer,
    getByIdBarang,
    getByIdSeller
}