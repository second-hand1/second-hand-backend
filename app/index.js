const express = require("express");
const bodyParser = require("body-parser");
const routerUsers = require("../config/routesUsers");
const routerTawaran= require("../config/routesTawaran");
const routerBarang = require("../config/routesBarang");
const routerTransaksi = require("../config/routesTransaksi");
const routerNotifikasi = require("../config/routesNotifkasi");
const multer = require("./midleware/multer");
const cloudinary = require("./thirdparty/cloudinary");
const cors = require("cors");
// const swaggerUI = require("swagger-ui-express");
// const yaml = require("js-yaml");
const fs = require("fs");
const path = require("path");

// const swaggerDocument = yaml.load(fs.readFileSync("swagger.yaml", "utf8"));
const app = express();
// const whitelist = ["http://localhost:3000"]
// const corsOptions = {
//   origin: function (origin, callback) {
//     if (!origin || whitelist.indexOf(origin) !== -1) {
//       callback(null, true)
//     } else {
//       callback(new Error("Not allowed by CORS"))
//     }
//   },
//   credentials: true,
// }
// app.use(cors(corsOptions));
app.use(cors());
app.use('/api/images', express.static(path.join(__dirname, '../images')));
app.use('/api/', express.static(path.join(__dirname, '../db/masterdata/')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(multer.multipleUpload)
// app.use(multer.upload.single('foto_produk'));
app.use(bodyParser.json());
// app.use("/api/api-docs", swaggerUI.serve, swaggerUI.setup(swaggerDocument));
app.use(routerUsers);
app.use(routerBarang);
app.use(routerTawaran);
app.use(routerTransaksi);
app.use(routerNotifikasi);

module.exports = app;
