'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Transaksi extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Transaksi.belongsTo(models.Barang, {foreignKey: 'id_barang', onDelete: 'CASCADE'});
      Transaksi.belongsTo(models.Tawaran, {foreignKey: 'id_tawaran', onDelete: 'CASCADE'});
      Transaksi.belongsTo(models.Users, {foreignKey: 'id_pembayar', onDelete: 'CASCADE'});
    }
  }
  Transaksi.init({
    kd_transaksi:DataTypes.STRING,
    id_barang: DataTypes.INTEGER,
    id_pembayar: DataTypes.INTEGER,
    id_tawaran: DataTypes.INTEGER,
    nominal_bayar: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Transaksi',
  });
  return Transaksi;
};