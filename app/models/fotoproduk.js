'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class fotoProduk extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      fotoProduk.belongsTo(models.Barang, {foreignKey: 'id_barang', onDelete: 'CASCADE'});
    }
  }
  fotoProduk.init({
    id_barang: DataTypes.INTEGER,
    foto_produk: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'fotoProduk',
  });
  return fotoProduk;
};