'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Users extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
     
    }
  }
  Users.init({
    nama: DataTypes.STRING,
    kota: DataTypes.STRING,
    alamat: DataTypes.TEXT,
    no_hp: DataTypes.STRING,
    foto_user: DataTypes.TEXT,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'Users',

  });
  return Users;
};