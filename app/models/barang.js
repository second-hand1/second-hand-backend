'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Barang extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Barang.belongsTo(models.Users, {foreignKey: 'id_seller', onDelete: 'CASCADE', hooks: true});
      
    }
  }
  Barang.init({
    nama_produk: DataTypes.TEXT,
    id_seller: DataTypes.INTEGER,
    harga_produk: DataTypes.INTEGER,
    kategori: DataTypes.STRING,
    deskripsi: DataTypes.TEXT,
    foto_produk: DataTypes.TEXT,

    status: DataTypes.ENUM('ditawar', 'terjual')

  }, {
    sequelize,
    modelName: 'Barang',
  });
  return Barang;
};