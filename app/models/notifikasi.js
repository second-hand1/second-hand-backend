'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Notifikasi extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Notifikasi.belongsTo(models.Tawaran, {foreignKey: 'id_tawaran', onDelete: 'CASCADE'});
      Notifikasi.belongsTo(models.Users, {foreignKey: 'id_user', onDelete: 'CASCADE'});
    }
  }
  Notifikasi.init({
    judul: DataTypes.TEXT,
    deskripsi: DataTypes.TEXT,
    id_user: DataTypes.INTEGER,
    id_tawaran: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Notifikasi',
  });
  return Notifikasi;
};