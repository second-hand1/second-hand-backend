'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Tawaran extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {

      Tawaran.belongsTo(models.Barang, {foreignKey: 'id_barang', onDelete: 'CASCADE'});
      Tawaran.belongsTo(models.Users, {foreignKey: 'id_buyer', onDelete: 'CASCADE'});
    }
  }
  Tawaran.init({
    harga_tawaran: DataTypes.INTEGER,
    id_buyer: DataTypes.INTEGER,
    id_barang: DataTypes.INTEGER,
    status : DataTypes.ENUM('disetujui', 'pending', 'tidak disetuji')
  }, {
    sequelize,
    modelName: 'Tawaran',
  });
  return Tawaran;
};