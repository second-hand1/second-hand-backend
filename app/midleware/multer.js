const multer = require("multer");
const fileStorage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'images');
    },
    filename: (req, file, cb) => {
        console.log("file: ", file);
        cb(null, new Date().getTime() + '-' + file.originalname)
    },
    
});

const fileFilter = (req, file, cb) => {
    if(file.mimetype === 'image/jpeg' || file.mimetype === 'image/png' || file.mimetype === 'image/jpg'){
        cb(null, true);
    }else{
        cb(null, false);
    }
};

var upload = multer({storage: fileStorage, fileFilter: fileFilter });
var multipleUpload = upload.array("foto", 5);

module.exports = { multipleUpload }